# CRL Moodle Theme - Based on Aardvark Theme by Shaun Daubney

Created by Dave Gnojek

www.kucrl.org

This theme has a fixed width. To remove the fixed width, delete "max-width: 960px;" in aardvark_layout.css

## Requirements

- Moodle 1.9.5+ (Tested - other versions untested)
- Firefox 3+, Safari 4+, IE7+ (others untested)
- Standard Theme

## Known Issues

- Menu not compatible with IE6 or earlier
- Main background colour and link colour not displaying in FF or Safari
