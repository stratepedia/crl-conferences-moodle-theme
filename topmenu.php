<div id="top_menu_date">
<a href="<?php echo $CFG->wwwroot.'/calendar/view.php' ?>"><script language="Javascript" type="text/javascript">
//<![CDATA[
<!--

// Get today's current date.
var now = new Date();

// Array list of days.
var days = new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

// Array list of months.
var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');

// Calculate the number of the current day in the week.
var date = ((now.getDate()<10) ? "0" : "")+ now.getDate();

// Calculate four digit year.
function fourdigits(number)     {
        return (number < 1000) ? number + 1900 : number;
                                                                }

// Join it all together
today =  days[now.getDay()] + " " +
              date + " " +
                          months[now.getMonth()] + " " +               
                (fourdigits(now.getYear())) ;

// Print out the data.
document.write("" +today+ " ");
  
//-->
//]]>
</script></a>
	
	</div>
    
<ul>
     
       <li class="home"><div><a href="<?php echo $CFG->wwwroot.'/' ?>"><img width="18" height="17" src="<?php echo $CFG->httpswwwroot.'/theme/'.current_theme() ?>/images/home_icon.png" alt=""/></a></div>
       </li> 
 
        <li><div><a href="<?php echo $CFG->wwwroot.'/' ?>">KU-CRL Resources</a>
        <ul>
     
        <li><a href="http://www.kucrl.org">KU-CRL Homepage</a></li>
		<li><a href="http://www.stratepedia.org">Stratepedia</a></li>
		<li><a href="http://www.kucrl.org/sim">SIM</a></li>
		<li><a href="http://clc.kucrl.org">CLC</a></li>
            </ul></div>
 
 		<li><div><a href="<?php echo $CFG->wwwroot.'/' ?>">KU-CRL Social Links</a>
        <ul>
     
        <li><a href="http://twitter.com/kucrl">KU-CRL on Twitter</a></li>
        <li><a href="http://www.youtube.com/kucrl">KU-CRL on YouTube</a></li>
		<li><a href="http://twitter.com/stratetweets">SIM on Twitter</a></li>
		<li><a href="http://twitter.com/stratepedia">Stratepedia on Twitter</a></li>
		            </ul></div>
 
 
 		<!--<li><div><a href="<?php echo $CFG->wwwroot.'/' ?>">CRL Projects</a>
        <ul>
     
        <li><a href="http://">Item 1</a></li>
		<li><a href="http://">Item 2</a></li>
		<li><a href="http://">Item 3</a></li>
		<li><a href="http://">Item 4</a></li>
		            </ul></div>
            
        <li><div><a href="<?php echo $CFG->wwwroot.'/' ?>">Menu Cuatro</a>
        <ul>
     
        <li><a href="http://">Item 1</a></li>
		<li><a href="http://">Item 2</a></li>
		<li><a href="http://">Item 3</a></li>
		<li><a href="http://">Item 4</a></li>            </ul></div>
 
 
 		<li><div><a href="<?php echo $CFG->wwwroot.'/' ?>">Menu Cinco</a>
        <ul>
     
        <li><a href="http://">Item 1</a></li>
		<li><a href="http://">Item 2</a></li>
		<li><a href="http://">Item 3</a></li>
		<li><a href="http://">Item 4</a></li>            </ul></div>-->
 
 
        